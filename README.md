# @krlwlfrt/tsg

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/tsg.svg?style=flat-square)](https://gitlab.com/krlwlfrt/tsg/commits/master) 
[![npm](https://img.shields.io/npm/v/@krlwlfrt/tsg.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/tsg)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/tsg.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/tsg)


This tool can generate TypeScript code from representations of objects with properties.

## Documentation

[See documentation](https://krlwlfrt.gitlab.io/tsg/) for detailed description of usage. 
