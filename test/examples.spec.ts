import {suite, test} from '@testdeck/mocha';
import {strictEqual} from 'assert';
import {generateName, generatePropertyType} from '../src/examples';

@suite()
export class ExamplesSpec {
  @test
  public generateName() {
    strictEqual(generateName({
      name: 'Foo',
      namespace: 'Bar',
    }), 'Bar_Foo');
  }

  @test
  public generateNameWithoutNamespace() {
    strictEqual(generateName({
      name: 'Foo',
    }), 'Foo');
  }

  @test
  public generatePropertyType() {
    strictEqual(generatePropertyType({
      name: 'Foo',
      namespace: 'Bar',
      type: {
        name: 'Foobar',
        namespace: 'Bar',
      },
    }), 'Bar_Foobar');
  }

  @test
  public generatePropertyTypeWithNamespaceEdm() {
    strictEqual(generatePropertyType({
      name: 'Foo',
      namespace: 'Bar',
      type: {
        name: 'Foobar',
        namespace: 'Edm',
      },
    }), 'foobar');
  }
}
