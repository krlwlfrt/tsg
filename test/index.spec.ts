import {suite, test} from '@testdeck/mocha';
import {doesNotThrow, strictEqual} from 'assert';
import * as typescript from 'typescript';
import {checkDuplicateNames, generateTypeScript} from '../src';
import {generateName, generatePropertyType} from '../src/examples';

@suite()
export class IndexSpec {
  @test
  public 'checkDuplicateNames: has duplicate'() {
    strictEqual(checkDuplicateNames([
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
      {
        name: 'Foo',
        namespace: 'Foobar',
        properties: [],
      },
    ]), true);
  }

  @test
  public 'checkDuplicateNames: has duplicate with namespace check'() {
    strictEqual(checkDuplicateNames([
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
    ], true), true);
  }

  @test
  public 'checkDuplicateNames: has no duplicate'() {
    strictEqual(checkDuplicateNames([
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
      {
        name: 'Foobar',
        namespace: 'Bar',
        properties: [],
      },
    ]), false);
  }

  @test
  public 'checkDuplicateNames: has no duplicate with empty list'() {
    strictEqual(checkDuplicateNames([], true), false);
  }

  @test
  public 'checkDuplicateNames: has no duplicate with namespace check'() {
    strictEqual(checkDuplicateNames([
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
      {
        name: 'Foo',
        namespace: 'Foobar',
        properties: [],
      },
    ], true), false);
  }

  @test
  public 'checkDuplicateNames: has no duplicate with short list'() {
    strictEqual(checkDuplicateNames([
      {
        name: 'Foo',
        namespace: 'Bar',
        properties: [],
      },
    ], true), false);
  }

  @test
  public generateTypeScript() {
    const generatedTypeScript = generateTypeScript([
      {
        description: 'A Foo object',
        name: 'Foo',
        namespace: 'Bar',
        properties: [
          {
            attributes: [
              {
                name: 'nullable',
                namespace: 'Bar',
                value: true,
              },
              {
                name: 'importance',
                namespace: 'Bar',
                value: 10,
              },
            ],
            multiple: true,
            name: 'Foo',
            namespace: 'Bar',
            type: {
              name: 'String',
              namespace: 'Edm',
            },
          },
          {
            multiple: true,
            name: 'Foo-bar',
            namespace: 'Bar',
            type: {
              name: 'Number',
              namespace: 'Edm',
            },
          },
        ],
      },
      {
        name: 'FooBar',
        namespace: 'Bar',
        parent: {
          name: 'Foo',
          namespace: 'Bar',
        },
        properties: [
          {
            description: 'An optional parameter',
            name: 'Bar',
            namespace: 'Bar',
            required: false,
            type: {
              name: 'String',
              namespace: 'Edm',
            },
          },
        ],
      },
      {
        name: 'Bar',
        namespace: 'Foo',
        type: {
          name: 'FooBar',
          namespace: 'Bar',
        },
      },
      {
        description: 'A type with a description',
        name: 'Lorem',
        namespace: 'Foo',
        multiple: true,
        type: {
          name: 'FooBar',
          namespace: 'Bar',
        },
      },
      {
        name: 'UnionFoo',
        namespace: 'Foo',
        unitedTypes: [
          'K',
          200,
          true,
          {
            name: 'UnionFoo',
            namespace: 'Foo',
            type: {
              name: 'string',
              namespace: 'Edm',
            },
          },
        ],
      },
      {
        name: 'EnumFoo',
        namespace: 'Foo',
        items: [
          {
            key: 'Foo',
            value: 'FooBar',
          },
          {
            key: 'Bar',
            value: 'Lorem',
          },
          {
            key: 'FooBar',
            value: false,
          },
        ],
      },
    ], generateName, generatePropertyType);

    strictEqual(generatedTypeScript, `/* eslint-disable */
/* tslint:disable */

/**
 * A Foo object
 */
export interface Bar_Foo {
  /**
   * @nullable true
   * @importance 10
   */
  Foo: Array<string | null>;
  'Foo-bar': number[];
}

export interface Bar_FooBar extends Bar_Foo {
  /**
   * An optional parameter
   */
  Bar?: string;
}

export type Foo_Bar = Bar_FooBar;

/**
 * A type with a description
 */
export type Foo_Lorem = Bar_FooBar[];

export type Foo_UnionFoo = 'K' | 200 | true | Foo_UnionFoo;

export enum Foo_EnumFoo {
  Foo = 'FooBar',
  Bar = 'Lorem',
  FooBar = false,
}
`);

    let code = '';

    doesNotThrow(() => {
      code = typescript.transpile(generatedTypeScript);
    });

    strictEqual(code !== '', true);
  }
}
