/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import { isAttribute, Property, ThingWithNameAndNamespace } from './index';

/**
 * Generate name for a thing
 * @param thing Thing to generate name for
 * @returns Name for thing
 */
export function generateName(thing: ThingWithNameAndNamespace): string {
  if (isAttribute(thing)) {
    return thing.name;
  }

  if (typeof thing.namespace !== 'string' || thing.namespace.length === 0) {
    return thing.name;
  }

  return `${thing.namespace}_${thing.name}`;
}

/**
 * Example implementation to generate property types
 * @param property Property to generate type for
 * @returns Name for property
 */
export function generatePropertyType(property: Property): string {
  let type = generateName(property.type);

  if (property.type.namespace === 'Edm') {
    type = property.type.name.toLowerCase();
  }

  if (Array.isArray(property.attributes)) {
    for (const attribute of property.attributes) {
      if (attribute.name === 'nullable' && typeof attribute.value === 'boolean' && attribute.value) {
        type += ' | null';
      }
    }
  }

  return type;
}
