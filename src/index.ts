/*
 * Copyright (C) 2019, 2020 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A thing with a name and a namespace
 */
export interface ThingWithNameAndNamespace {
  /**
   * An optional description for a thing
   */
  description?: string;

  /**
   * Name of the thing
   */
  name: string;

  /**
   * Namespace of the thing
   */
  namespace?: string;

  /**
   * Default value
   */
  value?: unknown;
}

/**
 * A type
 */
export type Type = Entity | Property | Union | Enum;

/**
 * An entity
 */
export interface Entity extends ThingWithNameAndNamespace {
  /**
   * Parent entity
   */
  parent?: ThingWithNameAndNamespace;

  /**
   * List of properties
   */
  properties: Property[];
}

/**
 * A property of an entity
 */
export interface Property extends ThingWithNameAndNamespace {
  /**
   * Additional attributes that further define the property
   */
  attributes?: Attribute[];

  /**
   * Whether or not the property can contain multiple instances
   */
  multiple?: boolean;

  /**
   * Whether or not the property is required
   */
  required?: boolean;

  /**
   * Type of the property
   */
  type: ThingWithNameAndNamespace;
}

/**
 * An attribute of a property
 */
export interface Attribute extends ThingWithNameAndNamespace {
  /**
   * Value of the attribute
   */
  value: boolean | number | string;
}

/**
 * A union of types
 */
export interface Union extends ThingWithNameAndNamespace {
  /**
   * List of united types
   */
  unitedTypes: (Type | string | number | boolean)[];
}

/**
 * An enumeration of values
 */
export interface Enum extends ThingWithNameAndNamespace {
  /**
   * List of values
   */
  items: {
    /**
     * Key of the item
     */
    key: string;
    /**
     * Value of the item
     */
    value: string | number | boolean;
  }[];
}

/**
 * Check if a Type is an Entity
 * @param type Type to check
 * @returns True, if Type is an Entity
 */
export function isEntity(type: Type): type is Entity {
  if (!('properties' in type)) {
    return false;
  }

  return Array.isArray(type.properties);
}

/**
 * Check if a Type is a Union
 * @param type Type to check
 * @returns True, if Type is Union
 */
export function isUnion(type: Type): type is Union {
  if (!('unitedTypes' in type)) {
    return false;
  }

  return Array.isArray(type.unitedTypes);
}

/**
 * Check if a Type is an Enum
 * @param type Type to check
 * @returns True, if Type is an Enum
 */
export function isEnum(type: Type): type is Enum {
  if (!('items' in type)) {
    return false;
  }

  return Array.isArray(type.items);
}

/**
 * Check if a ThingWithNameAndNamespace is an Attribute
 * @param thing Thing to check
 * @returns True, if ThingWithNameAndNamespace is an Attribute
 */
export function isAttribute(thing: ThingWithNameAndNamespace): thing is Attribute {
  if (!('value' in thing)) {
    return false;
  }

  return typeof thing.value !== 'undefined';
}

/**
 * Check for duplicate names
 * @param types List of types to check
 * @param checkNamespace Whether or not to check the namespace additionally to the name
 * @returns True, if duplicate names are found
 */
export function checkDuplicateNames(types: Type[], checkNamespace = false): boolean {
  // eslint-disable-next-line no-magic-numbers
  if (types.length < 2) {
    return false;
  }

  let duplicateFound = false;

  // eslint-disable-next-line no-magic-numbers
  for (let i = 0; i < types.length - 1; i++) {
    // eslint-disable-next-line no-magic-numbers
    for (let j = i + 1; j < types.length; j++) {
      if (types[i].name === types[j].name) {
        if (!checkNamespace || types[i].namespace === types[j].namespace) {
          console.info(`Found duplicate name '${types[i].name}' for pair [${i}, ${j}].`);
          duplicateFound = true;
        }
      }
    }
  }

  return duplicateFound;
}

/**
 * Quote property name if it is not alphanumerical
 * @param name Name to quote
 * @returns Quoted property name, if necessary
 */
function quotePropertyName(name: string): string {
  if (!/^[a-z0-9]*$/i.test(name)) {
    return `'${name.replace(/'/g, '\'')}'`;
  }

  return name;
}

/**
 * Compile types to TypeScript code
 * @param types List of types to compile
 * @param nameGenerator Function to generate the name of a thing
 * @param propertyTypeGenerator Function to generate the type of a property
 * @returns Generated TypeScript
 */
export function generateTypeScript(types: Type[],
                                   nameGenerator: (thing: ThingWithNameAndNamespace) => string,
                                   propertyTypeGenerator: (property: Property) => string): string {

  if (checkDuplicateNames(types)) {
    console.info('CAUTION: Duplicate names found!');
  }

  let output = `/* eslint-disable */
/* tslint:disable */
`;

  while (types.length > 0) {
    const type = types.shift() as Type;

    output += '\n';

    if (typeof type.description === 'string') {
      output += `/**
 * ${type.description}
 */\n`;
    }

    if (isEntity(type)) {
      output += `export interface ${nameGenerator(type)}`;

      if (typeof type.parent !== 'undefined') {
        output += ` extends ${nameGenerator(type.parent)}`;
      }

      output += ` {\n`;

      for (const property of type.properties) {
        if (typeof property.description === 'string'
          || (Array.isArray(property.attributes) && property.attributes.length > 0)) {
          output += `  /**\n`;

          if (typeof property.description === 'string') {
            output += `   * ${property.description}\n`;
          }

          if (Array.isArray(property.attributes)) {
            for (const attribute of property.attributes) {
              output += `   * @${nameGenerator(attribute)} ${attribute.value}\n`;
            }
          }

          output += `   */\n`;
        }

        output += `  ${quotePropertyName(property.name)}`;

        if (property.required === false) {
          output += '?';
        }

        const propertyType = propertyTypeGenerator(property);

        output += ': ';

        if (property.multiple === true) {
          if (propertyType.indexOf('|') >= 0) {
            output += `Array<${propertyType}>`;
          } else {
            output += `${propertyType}[]`;
          }
        } else {
          output += `${propertyType}`;
        }

        output += `;\n`;
      }

      output += `}\n`;
    } else if (isUnion(type)) {
      output += `export type ${nameGenerator(type)} = ${type.unitedTypes.map((unitedType) => {
        if (typeof unitedType === 'string') {
          return `'${unitedType}'`;
        }

        if (typeof unitedType === 'number' || typeof unitedType === 'boolean') {
          return unitedType;
        }

        return nameGenerator(unitedType);
      })
        .join(' | ')};
`;
    } else if (isEnum(type)) {
      output += `export enum ${nameGenerator(type)} {
`;

      for (const item of type.items) {
        let value = item.value;

        if (typeof value === 'string') {
          value = `'${value}'`;
        }

        output += `  ${item.key} = ${value},
`;
      }

      output += `}
`;
    } else {
      const propertyType = propertyTypeGenerator(type);

      output += `export type ${nameGenerator(type)} = `;

      if (type.multiple === true) {
        if (propertyType.indexOf('|') >= 0) {
          output += `Array<${propertyType}>`;
        } else {
          output += `${propertyType}[]`;
        }
      } else {
        output += `${propertyType}`;
      }

      output += `;\n`;
    }
  }

  return output;
}
