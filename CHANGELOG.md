# [1.1.0](https://gitlab.com/krlwlfrt/tsg/compare/v1.0.2...v1.1.0) (2024-11-25)



## [1.0.2](https://gitlab.com/krlwlfrt/tsg/compare/v1.0.1...v1.0.2) (2023-05-08)



## [1.0.1](https://gitlab.com/krlwlfrt/tsg/compare/v1.0.0...v1.0.1) (2023-05-08)



# [1.0.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.10.0...v1.0.0) (2023-02-28)



# [0.10.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.8.0...v0.10.0) (2021-07-26)



# [0.8.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.7.0...v0.8.0) (2021-03-05)



# [0.7.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.6.1...v0.7.0) (2021-02-22)



## [0.6.1](https://gitlab.com/krlwlfrt/tsg/compare/v0.6.0...v0.6.1) (2021-02-15)



# [0.6.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.5.1...v0.6.0) (2020-11-12)


### Features

* add possibility to have array types ([b4e0c13](https://gitlab.com/krlwlfrt/tsg/commit/b4e0c13e4528276d16dec447c53290db1d4dea1d))



## [0.5.1](https://gitlab.com/krlwlfrt/tsg/compare/v0.5.0...v0.5.1) (2020-05-11)



# [0.5.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.4.0...v0.5.0) (2020-03-06)


### Features

* add enum type and make namespace optional ([47fce6c](https://gitlab.com/krlwlfrt/tsg/commit/47fce6c0a2f3395d39c8d7f5aa3b02fe7ffba9f5))



# [0.4.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.3.1...v0.4.0) (2020-03-04)


### Bug Fixes

* remove extraneous file ([eff6da6](https://gitlab.com/krlwlfrt/tsg/commit/eff6da69a37fed83bda0419c82c1d49067df31c4))


### Features

* add union type ([cfc4c55](https://gitlab.com/krlwlfrt/tsg/commit/cfc4c55395d40fe6a4a8cfff5ec41f659d9fb001))



## [0.3.1](https://gitlab.com/krlwlfrt/tsg/compare/v0.3.0...v0.3.1) (2020-02-18)


### Bug Fixes

* adjust path ([544feee](https://gitlab.com/krlwlfrt/tsg/commit/544feee4da0bc5db8c1801a2483fcb31d9a23f46))



# [0.3.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.2.0...v0.3.0) (2020-02-18)


### Bug Fixes

* quote non alphanumerical property names ([65518b7](https://gitlab.com/krlwlfrt/tsg/commit/65518b747c3fc53bce16014ce59a00673742ab01))



# [0.2.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.1.0...v0.2.0) (2019-05-09)


### Features

* add attributes to description as annotations ([1b7cf39](https://gitlab.com/krlwlfrt/tsg/commit/1b7cf395ead062cfdf44a1d336acd54fc3fc969a))



# [0.1.0](https://gitlab.com/krlwlfrt/tsg/compare/v0.0.1...v0.1.0) (2019-05-08)


### Features

* add correct handling for required and multiple properties ([b2b3096](https://gitlab.com/krlwlfrt/tsg/commit/b2b3096bb094723a7c9b3664ba841f7b745bcb44))



## [0.0.1](https://gitlab.com/krlwlfrt/tsg/compare/8e4aa4a0da9318f142110403bcfd5e7a7004b950...v0.0.1) (2019-05-02)


### Features

* add implementation ([8e4aa4a](https://gitlab.com/krlwlfrt/tsg/commit/8e4aa4a0da9318f142110403bcfd5e7a7004b950))



