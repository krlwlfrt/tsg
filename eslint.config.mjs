import eslint from '@eslint/js';
import jsdoc from 'eslint-plugin-jsdoc';
import tseslint from 'typescript-eslint';


export default tseslint.config(
  eslint.configs.recommended,
  ...[jsdoc.configs['flat/recommended-typescript']],
  ...tseslint.configs.recommended,
  ...tseslint.configs.strict,
  ...tseslint.configs.stylistic,
  ...[
    {
      rules: {
        '@typescript-eslint/no-explicit-any': [
          'warn',
        ],
        'comma-dangle': [
          'error',
          'always-multiline',
        ],
        'no-duplicate-imports': [
          'error',
        ],
        'no-extra-semi': [
          'error',
        ],
        'no-magic-numbers': [
          'error',
          {
            'ignore': [
              0,
            ],
          },
        ],
        'no-unreachable': 'error',
        'object-curly-spacing': [
          'error',
          'always',
        ],
        'semi': [
          'error',
          'always',
        ],
        'semi-spacing': [
          'error',
        ],
        'semi-style': [
          'error',
        ],
      },
    },
  ],
);
